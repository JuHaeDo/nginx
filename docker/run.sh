#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
BUILDROOT=$DIR/..
 
cd $BUILDROOT
 
GIT_HEAD="$(git rev-parse --short=7 HEAD)"
GIT_DATE=$(git log HEAD -n1 --pretty='format:%cd' --date=format:'%Y%m%d-%H%M')
 
REPO="mytest" #harbor.emotibot.com/aarch64/bfop
CONTAINER="nginx"
TAG="${GIT_HEAD}-${GIT_DATE}"
 
BUILD_IMAGE_NAME="${REPO}/${CONTAINER}-build:${TAG}"
IMAGE_NAME="${REPO}/${CONTAINER}:${TAG}"
 
cmd="docker build -t $IMAGE_NAME -f $DIR/Dockerfile $BUILDROOT"
echo $cmd
eval $cmd

